angular.module('tvApp').directive('cinemaSlider', function () {
    return {
        restrict: 'E',
        scope: {
            focused: '='
        },
        templateUrl: 'views/partials/cinema-slider.html',
        controller: function ($scope, $http, $attrs, keyNav, $element) {
            $scope.title = $attrs.title;
            $scope.posters = [];
            var jSlides = $();
            var focusedSlide = 1;
            var jWrap = $($element).find('.slider-slides');

            $.get($attrs.src, function (response) {
                $scope.posters = response;
                $scope.$apply();
                jSlides = $($element).find('.slider-slide');
                focusSlide(focusedSlide);
            });

            function focusSlide (newFocus) {
                if (newFocus < 0) {
                    newFocus = 0
                }
                if (newFocus > jSlides.length - 1) {
                    newFocus = jSlides.length - 1;
                }

                focusedSlide = newFocus;

                var marginLeft = 280 + (-280 * focusedSlide);
                jWrap.css('margin-left', marginLeft);

                jSlides.removeClass('focused');
                jSlides.eq(focusedSlide).addClass('focused');
            }

            keyNav.bind('left', function () {
                if ($scope.focused) {
                    focusSlide(--focusedSlide)
                }
            });
            keyNav.bind('right', function () {
                if ($scope.focused) {
                    focusSlide(++focusedSlide)
                }
            });
        }
    }
});