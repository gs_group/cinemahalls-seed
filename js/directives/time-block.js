angular.module('tvApp').directive('timeBlock', function () {
    return {
        restrict: 'E',
        templateUrl: 'views/partials/time-block.html',
        controller: function ($scope) {
            $scope.time = Date.now();
            setInterval(function () {
                $scope.time = Date.now();
                $scope.$apply();
            }, 100)
        }
    }
});