var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');


gulp.task('sass', function() {
    gulp.src('./sass/styles.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./public'));
});

gulp.task('js', function() {
    return gulp.src([
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/moment/min/moment-with-locales.js',
        './node_modules/angular/angular.js',
        './node_modules/angular-moment/angular-moment.js',
        './node_modules/angular-route/angular-route.js',
        './js/*.js',
        './js/services/*.js',
        './js/controllers/*.js',
        './js/directives/*.js'
    ])
        .pipe(concat('bundle.js'))
        .pipe(gulp.dest('./public/'));
});

gulp.task('watch',function() {
    gulp.watch('./sass/**/*.scss', ['sass']);
    gulp.watch('./js/**/*.js', ['js']);
});